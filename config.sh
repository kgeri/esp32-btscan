#!/bin/bash -e

# START OF CONFIG
tty="/dev/ttyUSB0"
alert_address="d868c3ed165e"    # 6 bytes hexa
alert_gpio="16"                 # 0-255
scan_seconds="10"               # 0-65535
sleep_seconds="10"              # 0-65535
# END OF CONFIG

(read -n1024 -t5 RESP < $tty; echo $RESP)&
stty 115200 -F $tty
echo "$alert_address,$alert_gpio,$scan_seconds,$sleep_seconds" > $tty
