#!/bin/bash

target=$1

if [ target == "esp32" ]; then
    if [ -z "$IDF_PATH" ]; then
        . /media/tools/esp-idf/export.sh
    fi
fi

pio run --environment $target
