#include <unistd.h>

#include "bluetooth.h"
#include "config.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "led.h"
#include "scan_status.h"

#define TAG "main"
#define APP_ID "ESP32-scanner"
#define AWAKE_LED 4

void app_main(void) {
    led_init(AWAKE_LED, 1000);
    led_set_duty(4);  // Light up the on-board LED to signal boot

    // Loading the config
    Config* config = config_init(APP_ID);

    // Bringing the alert port down
    gpio_set_direction(config->alert_gpio, GPIO_MODE_OUTPUT);
    gpio_set_level(config->alert_gpio, 0);

    // Bringing up the Bluetooth stack
    bluetooth_init(APP_ID);

    bool alerting = false;
    while (true) {
        // Light up to signal discovery
        led_set_duty(1);

        // Discovering devices
        ESP_LOGI(TAG, "Discovering devices for %d seconds", config->scan_seconds);
        ScanStatus* status = bluetooth_scan(config->scan_seconds);

        // Checking if alert_address is gone
        BDAddress alert_address = config->alert_address;
        if (alert_address != 0) {
            if (!has_BDAddress(status, alert_address)) {
                ESP_LOGE(TAG, "Address gone: %llx! ALERTING", alert_address);
                alerting = true;
                led_init(AWAKE_LED, 1);
                led_set_duty(1);
                gpio_set_level(config->alert_gpio, 1);
            } else {
                ESP_LOGI(TAG, "Address is present: %llx", alert_address);
                alerting = false;
                led_init(AWAKE_LED, 1000);
                led_set_duty(1);
                gpio_set_level(config->alert_gpio, 0);
            }
        } else {
            ESP_LOGI(TAG, "Alert address not yet set");
        }

        // Turn off the led unless we're alerting
        if (!alerting) {
            led_set_duty(0);
        }

        ESP_LOGI(TAG, "Sleeping for %ds", config->sleep_seconds);
        sleep(config->sleep_seconds);
    }
}
