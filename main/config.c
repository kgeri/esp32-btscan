#include "config.h"

#include <inttypes.h>

#include "driver/uart.h"
#include "esp_log.h"
#include "nvs.h"
#include "nvs_flash.h"

#define TAG "config"
#define LOG_CONFIG(msg, c) ESP_LOGI(TAG, msg "Config[alert_address=%llx, alert_gpio=%u, scan_seconds=%u, sleep_seconds=%u]", \
                                    c.alert_address, c.alert_gpio, c.scan_seconds, c.sleep_seconds);

static Config config;
static QueueHandle_t uart_queue;

static void uart_init(const char* app_id);
static void uart_config_task(void* pvParameters);
static void update_config(const char* app_id, char* config_str);

Config* config_init(const char* app_id) {
    // Initialize NVS — it is used to store PHY calibration data as well as our config
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    // Load the config from flash on startup
    nvs_handle_t config_handle;
    ESP_ERROR_CHECK(nvs_open(app_id, NVS_READWRITE, &config_handle));

    if (nvs_get_u64(config_handle, "alert_address", &config.alert_address) != ESP_OK) {
        config.alert_address = 0;
    }
    if (nvs_get_u8(config_handle, "alert_gpio", &config.alert_gpio) != ESP_OK) {
        config.alert_gpio = 16;
    }
    if (nvs_get_u16(config_handle, "scan_seconds", &config.scan_seconds) != ESP_OK) {
        config.scan_seconds = 10;
    }
    if (nvs_get_u16(config_handle, "sleep_seconds", &config.sleep_seconds) != ESP_OK) {
        config.sleep_seconds = 10;
    }
    nvs_close(config_handle);

    // Log config on startup
    LOG_CONFIG("Loaded ", config);

    // Initialize the UART config change listener
    uart_init(app_id);

    return &config;
}

static void uart_init(const char* app_id) {
    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .rx_flow_ctrl_thresh = 122,
    };

    // Configure UART parameters
    ESP_ERROR_CHECK(uart_param_config(UART_NUM_0, &uart_config));
    ESP_ERROR_CHECK(uart_set_pin(UART_NUM_0, 1, 3, 0, 0));  // Using the default TX and RX pins, not using RTS/CTS

    // Setup UART buffered IO with event queue
    const int uart_buffer_size = (1024 * 2);

    // Install UART driver using an event queue here
    ESP_ERROR_CHECK(uart_driver_install(UART_NUM_0, uart_buffer_size, uart_buffer_size, 10, &uart_queue, 0));

    // Set uart pattern detect function for newlines
    uart_enable_pattern_det_baud_intr(UART_NUM_0, '\n', 1, 9, 0, 0);

    //Reset the pattern queue length to record at most 20 pattern positions
    uart_pattern_queue_reset(UART_NUM_0, 20);

    xTaskCreate(uart_config_task, "uart_config_task", 4096, (void*)app_id, 12, NULL);
}

static void uart_config_task(void* pvParameters) {
    const char* app_id = (const char*)pvParameters;
    uart_event_t event;
    char data[1024];
    while (true) {
        if (xQueueReceive(uart_queue, (void*)&event, (portTickType)portMAX_DELAY)) {
            if (event.type == UART_PATTERN_DET) {
                size_t len = 0;
                uart_get_buffered_data_len(UART_NUM_0, &len);
                int pos = uart_pattern_pop_pos(UART_NUM_0);
                if (pos > 0) {
                    uart_read_bytes(UART_NUM_0, data, pos, 100 / portTICK_PERIOD_MS);
                    data[pos] = '\0';  // Ensuring it's a string
                    update_config(app_id, data);
                }
                uart_flush_input(UART_NUM_0);
            }
        }
    }
    vTaskDelete(NULL);
}

static void update_config(const char* app_id, char* config_str) {
    // Parsing the config and updating in-memory
    if (sscanf(config_str, "%llx,%hhu,%hu,%hu",
               &config.alert_address,
               &config.alert_gpio,
               &config.scan_seconds,
               &config.sleep_seconds) != 4) {
        ESP_LOGE(TAG, "Failed to parse: %s", config_str);
        return;
    }

    // Store the config to flash
    nvs_handle_t config_handle;
    ESP_ERROR_CHECK(nvs_open(app_id, NVS_READWRITE, &config_handle));
    ESP_ERROR_CHECK(nvs_set_u64(config_handle, "alert_address", config.alert_address));
    ESP_ERROR_CHECK(nvs_set_u8(config_handle, "alert_gpio", config.alert_gpio));
    ESP_ERROR_CHECK(nvs_set_u16(config_handle, "scan_seconds", config.scan_seconds));
    ESP_ERROR_CHECK(nvs_set_u16(config_handle, "sleep_seconds", config.sleep_seconds));
    ESP_ERROR_CHECK(nvs_commit(config_handle));
    nvs_close(config_handle);
    LOG_CONFIG("Updated ", config);
}