#pragma once
#include <stdint.h>

#include "scan_status.h"

typedef struct Config {
    BDAddress alert_address;
    uint8_t alert_gpio;
    uint16_t scan_seconds;
    uint16_t sleep_seconds;
} Config;

Config* config_init(const char* app_id);
