#pragma once
#include <stdint.h>

void led_init(int gpio_num, uint32_t freq_hz);
void led_set_duty(uint32_t duty);
