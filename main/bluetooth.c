#include "bluetooth.h"

#include <math.h>
#include <string.h>
#include <unistd.h>

#include "esp_bt.h"
#include "esp_bt_device.h"
#include "esp_bt_main.h"
#include "esp_gap_bt_api.h"
#include "esp_log.h"

#define TAG "bluetooth"

static void bt_app_gap_cb(esp_bt_gap_cb_event_t event, esp_bt_gap_cb_param_t* param);
static void log_device(BDAddress address, esp_bt_gap_cb_param_t* param);

static ScanStatus scan_status;

void bluetooth_init(const char* app_id) {
    ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_BLE));

    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
    esp_err_t ret;
    if ((ret = esp_bt_controller_init(&bt_cfg)) != ESP_OK) {
        ESP_LOGE(TAG, "%s initialize controller failed: %s", __func__, esp_err_to_name(ret));
        return;
    }

    if ((ret = esp_bt_controller_enable(ESP_BT_MODE_CLASSIC_BT)) != ESP_OK) {
        ESP_LOGE(TAG, "%s enable controller failed: %s", __func__, esp_err_to_name(ret));
        return;
    }

    if ((ret = esp_bluedroid_init()) != ESP_OK) {
        ESP_LOGE(TAG, "%s initialize bluedroid failed: %s", __func__, esp_err_to_name(ret));
        return;
    }

    if ((ret = esp_bluedroid_enable()) != ESP_OK) {
        ESP_LOGE(TAG, "%s enable bluedroid failed: %s", __func__, esp_err_to_name(ret));
        return;
    }

    esp_bt_dev_set_device_name(app_id);

    esp_bt_gap_register_callback(bt_app_gap_cb);
    esp_bt_gap_set_scan_mode(ESP_BT_NON_CONNECTABLE, ESP_BT_NON_DISCOVERABLE);
}

ScanStatus* bluetooth_scan(uint16_t scan_seconds) {
    scan_status.discovered_count = 0;

    int remaining_inqs = scan_seconds / 1.28;  // See the idiotic API at esp_bt_gap_start_discovery()
    while (remaining_inqs > 0) {
        uint8_t inq_len = fmin(0x30, remaining_inqs);

        scan_status.discovery_completed = false;
        esp_bt_gap_start_discovery(ESP_BT_INQ_MODE_GENERAL_INQUIRY, inq_len, 0);
        while (!scan_status.discovery_completed) {
            sleep(1);
        }

        remaining_inqs -= inq_len;
    }
    return &scan_status;
}

static void bt_app_gap_cb(esp_bt_gap_cb_event_t event, esp_bt_gap_cb_param_t* param) {
    switch (event) {
        case ESP_BT_GAP_DISC_RES_EVT: {
            BDAddress address = new_BDAddress(param->disc_res.bda);
            log_device(address, param);
            add_BDAddress(&scan_status, address);
            break;
        }
        case ESP_BT_GAP_DISC_STATE_CHANGED_EVT: {
            if (param->disc_st_chg.state == ESP_BT_GAP_DISCOVERY_STOPPED) {
                ESP_LOGI(TAG, "Discovery stopped.");
                scan_status.discovery_completed = true;
            } else if (param->disc_st_chg.state == ESP_BT_GAP_DISCOVERY_STARTED) {
                ESP_LOGI(TAG, "Discovery started");
            }
            break;
        }
        default:
            break;
    }
    return;
}

static void log_device(BDAddress address, esp_bt_gap_cb_param_t* param) {
    uint8_t rssi = 0;
    char bdname[ESP_BT_GAP_MAX_BDNAME_LEN + 1];
    bdname[0] = '\0';

    for (size_t i = 0; i < param->disc_res.num_prop; i++) {
        esp_bt_gap_dev_prop_t prop = param->disc_res.prop[i];

        if (prop.type == ESP_BT_GAP_DEV_PROP_RSSI) {
            // Note RSSI is actually an int8_t, but casting it to uint to make it easier to read (0-255)
            rssi = *(uint8_t*)prop.val;
        } else if (prop.type == ESP_BT_GAP_DEV_PROP_EIR) {
            uint8_t* eir = (uint8_t*)(prop.val);
            uint8_t eir_bdname_len;
            uint8_t* eir_bdname = esp_bt_gap_resolve_eir_data(eir, ESP_BT_EIR_TYPE_CMPL_LOCAL_NAME, &eir_bdname_len);
            if (!eir_bdname) {
                eir_bdname = esp_bt_gap_resolve_eir_data(eir, ESP_BT_EIR_TYPE_SHORT_LOCAL_NAME, &eir_bdname_len);
            }
            if (!eir_bdname) {
                continue;
            }
            memcpy(bdname, eir_bdname, eir_bdname_len);
            bdname[eir_bdname_len] = '\0';
        }
    }

    ESP_LOGI(TAG, "Discovered: %llx (name=%s, RSSI=%u)", address, bdname, rssi);
}