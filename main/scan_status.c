#include "scan_status.h"

BDAddress new_BDAddress(uint8_t address[6]) {
    return ((uint64_t)address[0]) << 40 |
           ((uint64_t)address[1]) << 32 |
           ((uint64_t)address[2]) << 24 |
           ((uint64_t)address[3]) << 16 |
           ((uint64_t)address[4]) << 8 |
           address[5];
}

ScanStatus *add_BDAddress(ScanStatus *status, BDAddress address) {
    if (!has_BDAddress(status, address)) {
        status->discovered[status->discovered_count++] = address;
    }
    return status;
}

bool has_BDAddress(ScanStatus *status, BDAddress address) {
    for (size_t i = 0; i < status->discovered_count && i < MAX_DISCOVERED_COUNT; i++) {
        if (status->discovered[i] == address) {
            return true;
        }
    }
    return false;
}