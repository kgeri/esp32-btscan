#pragma once
#include <stdint.h>

#include "scan_status.h"

void bluetooth_init(const char* app_id);
ScanStatus* bluetooth_scan(uint16_t scan_seconds);