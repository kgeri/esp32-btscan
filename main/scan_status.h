#pragma once
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef uint64_t BDAddress;

#define MAX_DISCOVERED_COUNT 64
typedef struct ScanStatus {
    bool discovery_completed;
    BDAddress discovered[MAX_DISCOVERED_COUNT];
    size_t discovered_count;
} ScanStatus;

BDAddress new_BDAddress(uint8_t address[6]);
ScanStatus *add_BDAddress(ScanStatus *status, BDAddress address);
bool has_BDAddress(ScanStatus *status, BDAddress address);