#!/bin/bash -e

if [ ! -f $(which esptool.py) ]; then
    pip3 install esptool
fi

esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash 0x10000 .pio/build/esp32/firmware.bin
